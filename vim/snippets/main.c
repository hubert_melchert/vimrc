#include <stdio.h>
#include <stdint.h>

#define NELEMS(array) (sizeof(array) / sizeof(array[0]))

void print_array(uint8_t *array, int nelems)
{
    for(int i = 0; i < nelems; i++)
    {
        printf("%d ", array[i]);
    }
    printf("\n");
}

int main(int argc, char *argv[])
{
    
    return 0;
}
