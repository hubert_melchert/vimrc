" Autocomplete

" Tabs
set expandtab
set tabstop=4
set autoread
set ruler

" Autoindent
set shiftwidth=4
set autoindent
set smartindent

" Autocompletion
map <C-F12> :!ctags -R --sort=1 --c-types=+lcdefgmnpstuvx --fields=+iaS --extra=+q --exclude=.git --exclude=build. <CR>
let OmniCpp_GlobalScopeSearch = 1      
let OmniCpp_ShowAccess = 1      
let OmniCpp_ShowPrototypeInAbbr = 1
let OmniCpp_MayCompleteDot = 1
let OmniCpp_MayCompleteArrow = 1
let OmniCpp_MayCompleteScope = 1
au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
set completeopt=menuone,menu,longest,preview
" ENTER confirms selection from popup
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

highlight Pmenu ctermbg=238 gui=bold
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
inoremap <expr> <C-n> pumvisible() ? '<C-n>' :
  \ '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'

inoremap <expr> <M-,> pumvisible() ? '<C-n>' :
  \ '<C-x><C-o><C-n><C-p><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'
inoremap <C-w> <C-x><C-o>
" automatically open and close the popup menu / preview window      
au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif      
inoremap <Nul> <C-x><C-o> 
inoremap <expr> <c-j> ("\<C-n>")
inoremap <expr> <c-k> ("\<C-p>")
" Comments
nmap <C-c> I//<Esc>
" Block comment"
vmap <C-c> :s/^/\/\//<Esc><Esc>:nohl<CR>


" Cpp Check
nnoremap <C-b> :Cppcheck<CR>:redraw!<CR>

" OmniCppComplete initialization
call omni#cpp#complete#Init()
