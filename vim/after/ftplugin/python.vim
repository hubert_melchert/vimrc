set autoindent
set encoding=utf-8
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set fileformat=unix
set smartindent

" Comments
nmap <C-c> I#<Esc>
" Block comment"
vmap <C-c> :s/^/#<Esc><Esc>:nohl<CR>

let python_highlight_all = 1
