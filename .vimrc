" VUNDLE START
set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" PLUGINS
Plugin 'vim-scripts/OmniCppComplete'
Plugin 'dhruvasagar/vim-table-mode'
Plugin 'kien/ctrlp.vim'
Plugin 'vim-scripts/Align'
Plugin 'dkprice/vim-easygrep'
Plugin 'tpope/vim-fugitive.git'
Plugin 'junegunn/gv.vim.git'
Plugin 'scrooloose/nerdtree.git'
Plugin 'majutsushi/tagbar'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-scripts/a.vim'
Plugin 'mbbill/code_complete'
Plugin 'rust-lang/rust.vim'
Plugin 'octol/vim-cpp-enhanced-highlight'
Plugin 'talek/obvious-resize'
Plugin 'drmikehenry/vim-headerguard'
call vundle#end()
filetype plugin indent on
" VUNDLE END

set backspace=indent,eol,start
" Fix backspace/delete problem
syntax on
colorscheme slate
filetype plugin on
set nocp
set nowrap

" nnoremap <Space> <Nop>
nnoremap <Space> <Nop>
let mapleader = " "

" Prevent entering Ex mode
nnoremap Q <nop> 
" Command completion
set wildmenu
set wildmode=list:longest,full

" 80 characters line
highlight ColorColumn ctermbg=magenta
call matchadd('ColorColumn', '\%81v', 100)

" Obvious resize
noremap <silent> <C-Up> :<C-U>ObviousResizeUp<CR>
noremap <silent> <C-Down> :<C-U>ObviousResizeDown<CR>
noremap <silent> <C-Left> :<C-U>ObviousResizeLeft<CR>
noremap <silent> <C-Right> :<C-U>ObviousResizeRight<CR>

" Search options
set showmatch
set incsearch
set hlsearch
set ignorecase
set smartcase
set path+=**
map <C-h> :nohl<CR>
" Ctrl+P
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard && git submodule foreach "git ls-files -co --exclude-standard | while read i; do echo \"\$path/\$i\"; done"']
set wildignore+=tags,*.o,*.obj,*.su,*.a1
let g:ctrlp_cmd='CtrlP :pwd'
let g:ctrl_use_caching=0

let g:EasyGrepCommand=1
let g:EasyGrepFilesToExclude="tags,*.o,*.obj,*.su,*.list,Debug"
let g:EasyGrepRecursive=1
let g:EasyGrepAllOptionsInExplorer=1
:autocmd FileType qf wincmd J
" Scroll
set scrolloff=10

" Normal mode
nnoremap ; :
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==

" Insert mode
inoremap <C-j> <ESC>:m .+1<CR>==gi
inoremap <C-k> <ESC>:m .-2<CR>==gi

" Visual mode
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv

" Highlighting
set cursorline
highlight CursorLine cterm=NONE ctermbg=238

" Line numbering
set number relativenumber

augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

" File explorer
nnoremap <C-l> :NERDTreeToggle<CR>
inoremap <C-l> :<Esc>NERDTreeToggle<CR>
vnoremap <C-l> :<Esc>NERDTreeToggle<CR>

" Splits
set splitright
set splitbelow
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>
" Cheatsheet
nnoremap <F2> :split ~/.vim_cheatsheet.txt <CR>
" Statusline
let g:airline#extensions#whitespace#enabled = 0
let g:airline#extensions#tabline#enabled = 1
" Alarms
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=

" Auto close brackets
inoremap {      {}<Left>
inoremap {<CR>  {<CR>}<CR><Esc>kO
inoremap {{     {
inoremap {}     {}
inoremap <expr> }  strpart(getline('.'), col('.')-1, 1) == "}" ? "\<Right>" : "}"

inoremap (      ()<Left>
inoremap ((     (
inoremap ()     ()
inoremap <expr> )  strpart(getline('.'), col('.')-1, 1) == ")" ? "\<Right>" : ")"

inoremap [      []<Left>
inoremap [[     [
inoremap []     []
inoremap <expr> ]  strpart(getline('.'), col('.')-1, 1) == "]" ? "\<Right>" : "]"

inoremap "      ""<Left>
inoremap ""     "
inoremap <expr> " strpart(getline('.'), col('.')-1, 1) == "\"" ? "\<Right>" : "\""

imap <C-d> <C-O>%<C-O>%<right>
" Tags list

map <F4> <Esc> :TagbarToggle<CR> 
" Snippets
command NewC :-1read ~/.vim/snippets/main.c


" Copy and paste
:set pastetoggle=<F3>
:vnoremap <C-y> "+y
:vnoremap <C-d> "+d

" Vim Table Mode
let g:table_mode_tableize_map='\tz'
let g:table_mode_corner_corner='+'
let g:table_mode_header_fillchar='=' 

" Align
map <C-a> :Align = :<CR>

" Typos
command-bang Q q<bang>
command W w
command-bang Qa qa<bang>
command Wqa wqa

" Testing
command Test ! ./test.sh %
set lazyredraw

" Splits
set splitbelow
set splitright

" Surrounding
nnoremap <leader>w( bcw()<Esc>P
nnoremap <leader>w{ bcw{}<Esc>P
nnoremap <leader>w[ bcw[]<Esc>P
nnoremap <leader>w" bcw""<Esc>P
nnoremap <leader>w/ bcw/**/<Esc>hP

" Git
autocmd! FileType git setlocal foldlevel=1

" camel case -> underscore
nnoremap cu :s#\(\<\u\l\+\|\l\+\)\(\u\)#\l\1_\l\2#g<CR>


" Let's save undo info!
if !isdirectory($HOME."/.vim")
    call mkdir($HOME."/.vim", "", 0770)
endif
if !isdirectory($HOME."/.vim/undo-dir")
    call mkdir($HOME."/.vim/undo-dir", "", 0700)
endif
set undodir=~/.vim/undo-dir
set undofile

" Automatically open quickfix
autocmd QuickFixCmdPost [^l]* nested cwindow
autocmd QuickFixCmdPost    l* nested lwindow
