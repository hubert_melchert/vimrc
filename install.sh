echo "Updating vim settings"
echo "#####################"
echo "Removing all plugins"
rm -rf ~/.vim/
echo "Copying .vimrc"
cp .vimrc ~/.vimrc
echo "Copying .vim/"
mkdir ~/.vim
cp -R  vim/* ~/.vim
echo "Installing Vundle"
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

echo "Copying CppChecker"
mkdir ~/.vim/ftplugin
cp c_cppcheck.vim  ~/.vim/ftplugin/c_cppcheck.vim

vim +PluginInstall +qall
